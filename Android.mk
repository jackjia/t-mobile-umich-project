LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS += -fPIE
LOCAL_LDFLAGS += -fPIE -pie

LOCAL_SRC_FILES := \
udp_inject.c

LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_LDLIBS := -ldl -llog 

LOCAL_STATIC_LIBRARIES := libc

LOCAL_MODULE := udp_inject
LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)
LOCAL_MODULE_TAGS := debug

include $(BUILD_EXECUTABLE)
